<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get('/', function (Request $request, Response $response, $args) {
    // Render index view
    return $this->view->render($response, 'index.latte');
})->setName('index');


$app->post('/test', function (Request $request, Response $response, $args) {
    //read POST data
    $input = $request->getParsedBody();

    //log
    $this->logger->info('Your name: ' . $input['person']);

    return $response->withHeader('Location', $this->router->pathFor('index'));
})->setName('redir');


/* Vypis osob */
$app->get('/persons', function (Request $request, Response $response, $args) {
	// V premennej stmt je ulozeny vysledok dotazu ako objekt databazoveho konektoru
	$stmt = $this->db->query('SELECT * FROM person ORDER BY first_name');

	// Je potrebne previest na datovu strukturu jazyka php
	$tplVars['osoby'] = $stmt->fetchAll();
	/*
	osoby => [
	$o=> 	[first_name => "Pepa", last_name => "Novak", "height" => 156]
		[first_name => "Jan", last_name => "B", "height" => 156]
	]
	*/
    return $this->view->render($response, 'persons.latte', $tplVars);
})->setName('persons');

